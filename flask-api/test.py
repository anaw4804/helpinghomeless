import os
import sys
import unittest
from models import db, Demographic, FoodBank, States, Shelters


class DBTestCases(unittest.TestCase):

    # -----------
    # Food table.
    # -----------

    def test_food_1(self):
        db.session.query(FoodBank).filter_by(id=200).delete()
        db.session.query(Demographic).filter_by(id=200).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        s = FoodBank(id=200, charityName="testing1", charityNavigatorURL="charityNavigatorURL", rating=5, city="city", country="country", postalCode="postalCode",
                     stateOrProvince=stateOrProvince.abbreviation, streetAddress1="streetAddress1", streetAddress2="streetAddress2", mission="mission", tagLine="tagLine", websiteURL="websiteURL")
        db.session.add(s)
        db.session.commit()

        r = db.session.query(FoodBank).filter_by(id=200).one()
        self.assertEqual(str(r.id), '200')
        self.assertEqual(str(r.rating), '5')

        db.session.query(FoodBank).filter_by(id=200).delete()
        db.session.commit()
        db.session.close()

    def test_food_2(self):
        db.session.query(Demographic).filter_by(id=202).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()

        s = FoodBank(id=202, charityName="testing2", charityNavigatorURL="charityNavigatorURL", rating=5, city="city", country="country", postalCode="postalCode",
                     stateOrProvince=stateOrProvince.abbreviation, streetAddress1="streetAddress1", streetAddress2="streetAddress2", mission="mission", tagLine="tagLine", websiteURL="websiteURL")
        db.session.add(s)
        db.session.commit()

        r = db.session.query(FoodBank).filter_by(id=202).one()
        self.assertEqual(str(r.tagLine), 'tagLine')
        self.assertEqual(str(r.postalCode), 'postalCode')

        db.session.query(FoodBank).filter_by(id=202).delete()
        db.session.commit()
        db.session.close()

    def test_food_3(self):
        db.session.query(Demographic).filter_by(id=203).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        s = FoodBank(id=203, charityName="testing3", charityNavigatorURL="charityNavigatorURL", rating=5, city="city", country="country", postalCode="postalCode",
                     stateOrProvince=stateOrProvince.abbreviation, streetAddress1="streetAddress1", streetAddress2="streetAddress2", mission="mission", tagLine="tagLine", websiteURL="websiteURL")
        db.session.add(s)
        db.session.commit()

        r = db.session.query(FoodBank).filter_by(id=203).one()
        self.assertEqual(str(r.charityName), 'testing3')
        self.assertEqual(str(r.mission), 'mission')

        db.session.query(FoodBank).filter_by(id=203).delete()
        db.session.commit()
        db.session.close()

    def test_food_4(self):

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        s = FoodBank(id=210, charityName="testing4", charityNavigatorURL="charityNavigatorURL", rating=5, city="city", country="country", postalCode="postalCode",
                     stateOrProvince=stateOrProvince.abbreviation, streetAddress1="streetAddress1", streetAddress2="streetAddress2", mission="mission", tagLine="tagLine", websiteURL="websiteURL")
        db.session.add(s)
        db.session.commit()

        r = db.session.query(FoodBank).filter_by(id=210).one()
        self.assertEqual(str(r.charityName), 'testing4')
        self.assertEqual(str(r.mission), 'mission')

        db.session.query(FoodBank).filter_by(id=210).delete()
        db.session.commit()
        db.session.close()

    def test_food_5(self):

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        s = FoodBank(id=211, charityName="testing5", charityNavigatorURL="charityNavigatorURL", rating=5, city="city", country="country", postalCode="postalCode",
                     stateOrProvince=stateOrProvince.abbreviation, streetAddress1="streetAddress1", streetAddress2="streetAddress2", mission="mission", tagLine="tagLine", websiteURL="websiteURL")
        db.session.add(s)
        db.session.commit()

        r = db.session.query(FoodBank).filter_by(id=211).one()
        self.assertEqual(str(r.charityName), 'testing5')
        self.assertEqual(str(r.mission), 'mission')

        db.session.query(FoodBank).filter_by(id=211).delete()
        db.session.commit()
        db.session.close()

    def test_food_6(self):

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        s = FoodBank(id=212, charityName="testing6", charityNavigatorURL="charityNavigatorURL", rating=5, city="city", country="country", postalCode="postalCode",
                     stateOrProvince=stateOrProvince.abbreviation, streetAddress1="streetAddress1", streetAddress2="streetAddress2", mission="mission", tagLine="tagLine", websiteURL="websiteURL")
        db.session.add(s)
        db.session.commit()

        r = db.session.query(FoodBank).filter_by(id=212).one()
        self.assertEqual(str(r.charityName), 'testing6')
        self.assertEqual(str(r.mission), 'mission')

        db.session.query(FoodBank).filter_by(id=212).delete()
        db.session.commit()
        db.session.close()

    # -------------------
    # Demographics table.
    # -------------------

    def test_demographics_1(self):
        db.session.query(Demographic).filter_by(id=3002).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newStat = Demographic(id=3002, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=3002).one()
        self.assertEqual(str(r.id), '3002')
        self.assertEqual(str(r.year), 'year')

        db.session.query(Demographic).filter_by(id=3002).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_2(self):
        db.session.query(Demographic).filter_by(id=3012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newStat = Demographic(id=3012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=3012).one()
        self.assertEqual(str(r.id), '3012')
        self.assertEqual(str(r.overall), 'overall')
        self.assertEqual(str(r.SH), 'SH')

        db.session.query(Demographic).filter_by(id=3012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_3(self):
        db.session.query(Demographic).filter_by(id=4012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newStat = Demographic(id=4012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=4012).one()
        self.assertEqual(str(r.id), '4012')
        self.assertEqual(str(r.homeless_families), 'homeless_families')
        self.assertEqual(str(r.ES), 'ES')

        db.session.query(Demographic).filter_by(id=4012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_4(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertEqual(str(r.id), '5012')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_5(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertEqual(str(r.state), 'AK')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_6(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertEqual(str(r.year), 'year')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_7(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertEqual(str(r.overall), 'overall')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_8(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertEqual(str(r.ES), 'ES')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_8_inverted_test(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertNotEqual(str(r.ES), '')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()


    def test_demographics_9(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertEqual(str(r.TH), 'TH')

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    def test_demographics_9_inverted_test(self):
        db.session.query(Demographic).filter_by(id=5012).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="AK").one()
        newStat = Demographic(id=5012, state=stateOrProvince.abbreviation, year="year", overall="overall", ES="ES", TH="TH", SH="SH", sheltered_total="sheltered_total", unsheltered_homeless="unsheltered_homeless", homeless_individuals="homeless_individuals", ES_individuals="ES_individuals", TH_individuals="TH_individuals", SH_individuals="SH_individuals",
                              sheltered_total_individuals="sheltered_total_individuals", unsheltered_homeless_individuals="unsheltered_homeless_individuals", homeless_families="homeless_families", ES_families="ES_families", TH_families="TH_families", sheltered_families="sheltered_families", unsheltered_families="unsheltered_families", homeless_households="homeless_households")

        db.session.add(newStat)
        db.session.commit()

        r = db.session.query(Demographic).filter_by(id=5012).one()
        self.assertNotEqual(str(r.TH), "")

        db.session.query(Demographic).filter_by(id=5012).delete()
        db.session.commit()
        db.session.close()

    # -------------------
    # Shelter table.
    # -------------------

    def test_shelters_1(self):
        db.session.query(Shelters).filter_by(objectid=3002).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=3002, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=234, longitude=2343, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=3002).one()
        self.assertEqual(str(r.objectid), '3002')
        self.assertEqual(str(r.zip), 'zip')
        self.assertEqual(str(r.latitude), '234')
        self.assertEqual(str(r.longitude), '2343')

        db.session.query(Shelters).filter_by(objectid=3002).delete()
        db.session.commit()
        db.session.close()

    def test_shelters_2(self):
        db.session.query(Shelters).filter_by(objectid=78787).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=78787, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=234, longitude=2343, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=78787).one()
        self.assertEqual(str(r.objectid), '78787')
        self.assertEqual(str(r.zip), 'zip')
        self.assertEqual(str(r.agesServed), "agesServed")
        self.assertEqual(str(r.longitude), '2343')

        db.session.query(Shelters).filter_by(objectid=78787).delete()
        db.session.commit()
        db.session.close()

    def test_shelters_3(self):
        r = db.session.query(Shelters).filter_by(objectid=34535).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=34535, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=67, longitude=2343, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=34535).one()
        self.assertEqual(str(r.objectid), '34535')
        self.assertEqual(str(r.facilityName), 'facilityName')
        self.assertEqual(str(r.latitude), '67')
        self.assertEqual(str(r.city), 'city')

        db.session.query(Shelters).filter_by(objectid=34535).delete()
        db.session.commit()
        db.session.close()

        def test_shelters_4(self):
            r = db.session.query(Shelters).filter_by(objectid=81239).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=81239, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=70, longitude=1000, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=81239).one()
        self.assertEqual(str(r.objectid), '81239')
        self.assertEqual(str(r.facilityName), 'facilityName')
        self.assertEqual(str(r.latitude), '70')
        self.assertEqual(str(r.city), 'city')

        db.session.query(Shelters).filter_by(objectid=81239).delete()
        db.session.commit()
        db.session.close()

        def test_shelters_5(self):
            r = db.session.query(Shelters).filter_by(objectid=10000).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=10000, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=21, longitude=8921, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=10000).one()
        self.assertEqual(str(r.objectid), '10000')
        self.assertEqual(str(r.facilityName), 'facilityName')
        self.assertEqual(str(r.latitude), '21')
        self.assertEqual(str(r.city), 'city')

        db.session.query(Shelters).filter_by(objectid=10000).delete()
        db.session.commit()
        db.session.close()

        def test_shelters_6(self):
            r = db.session.query(Shelters).filter_by(objectid=98743).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=98743, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=25, longitude=1000, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=98743).one()
        self.assertEqual(str(r.objectid), '98743')
        self.assertEqual(str(r.facilityName), 'facilityName')
        self.assertEqual(str(r.latitude), '25')
        self.assertEqual(str(r.city), 'city')

        db.session.query(Shelters).filter_by(objectid=98743).delete()
        db.session.commit()
        db.session.close()

        def test_shelters_7(self):
            r = db.session.query(Shelters).filter_by(objectid=43531).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=43531, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=50, longitude=1000, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=43531).one()
        self.assertEqual(str(r.objectid), '43531')
        self.assertEqual(str(r.facilityName), 'facilityName')
        self.assertEqual(str(r.latitude), '50')
        self.assertEqual(str(r.city), 'city')

        db.session.query(Shelters).filter_by(objectid=43531).delete()
        db.session.commit()
        db.session.close()

        def test_shelters_8(self):
            r = db.session.query(Shelters).filter_by(objectid=78787).delete()

        stateOrProvince = db.session.query(
            States).filter_by(abbreviation="TX").one()
        newshelter = Shelters(objectid=78787, ownerRenter="ownerRenter", facilityName="facilityName", provider="provider",
                              address="address", city="city", state=stateOrProvince.abbreviation, zip="zip", latitude=45, longitude=1000, subtype="subtype",
                              status="status", numberOfBeds=10, agesServed="agesServed", howToAccess="howToAccess", LGBTQServed="LGBTQServed")

        db.session.add(newshelter)
        db.session.commit()

        r = db.session.query(Shelters).filter_by(objectid=78787).one()
        self.assertEqual(str(r.objectid), '78787')
        self.assertEqual(str(r.facilityName), 'facilityName')
        self.assertEqual(str(r.latitude), '45')
        self.assertEqual(str(r.city), 'city')

        db.session.query(Shelters).filter_by(objectid=78787).delete()
        db.session.commit()
        db.session.close()

if __name__ == '__main__':  # pragma: no cover
    unittest.main()
