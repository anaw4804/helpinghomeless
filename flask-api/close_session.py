from models import app, db

def close_session():
    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {
    "max_overflow": 15,
    "pool_pre_ping": True,
    "pool_recycle": 60 * 60,
    "pool_size": 30,
    "pool_timeout": 300,
}
    db.session.remove()
