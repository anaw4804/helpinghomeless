#!/usr/bin/env python3

import json
from models import app, db, FoodBank, Shelters, Demographic, States, Channels

# ------------
# load_json
# ------------


def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


def create_states():
    """"
    populating states table
    """
    states = load_json('states.json')
    for state in states:
        name = state['name']
        abbreviation = state['abbreviation']

        newState = States(name=name, abbreviation=abbreviation)

        db.session.add(newState)
        db.session.commit()
    db.session.close()
# -----------------
# create_food_banks
# -----------------


def create_food_banks():
    """
    populate food banks table
    """
    foodBanks = load_json('foodResources.json')
    index = 1
    for oneBank in foodBanks:
        id = index
        charityName = oneBank['charityName']
        charityNavigatorURL = oneBank['charityNavigatorURL']
        rating = oneBank['currentRating']['rating']
        city = oneBank['mailingAddress']['city']
        country = oneBank['mailingAddress']['country']
        postalCode = oneBank['mailingAddress']['postalCode']
        identifer = oneBank['mailingAddress']['stateOrProvince']
        stateOrProvince = db.session.query(
            States).filter_by(abbreviation=identifer).one()
        streetAddress1 = oneBank['mailingAddress']['streetAddress1']
        streetAddress2 = oneBank['mailingAddress']['streetAddress2']
        mission = oneBank['mission']
        tagLine = oneBank['tagLine']
        websiteURL = oneBank['websiteURL']

        newFoodBank = FoodBank(id=id, charityName=charityName, charityNavigatorURL=charityNavigatorURL, rating=rating, city=city, country=country, postalCode=postalCode,
                               stateOrProvince=stateOrProvince.abbreviation, streetAddress1=streetAddress1, streetAddress2=streetAddress2, mission=mission, tagLine=tagLine, websiteURL=websiteURL)

        # After I create the foodBank, I can then add it to my session.
        db.session.add(newFoodBank)
        # commit the session to my DB.
        db.session.commit()

        index += 1
    db.session.close()

# -----------------
# create_demographics
# -----------------


def create_demographics():
    """
    populate demographics table
    """
    demo = load_json('demographics.json')
    index = 1
    for stat in demo:
        id = index
        identifer = stat
        state = db.session.query(States).filter_by(
            abbreviation=identifer).one()
        year = demo[stat]['year']
        overall = demo[stat]['Overall Homeless']
        ES = demo[stat]['Sheltered ES Homeless']
        TH = demo[stat]['Sheltered TH Homeless']
        SH = demo[stat]['Sheltered SH Homeless']
        sheltered_total = demo[stat]['Sheltered Total Homeless']
        unsheltered_homeless = demo[stat]['Unsheltered Homeless']
        homeless_individuals = demo[stat]['Homeless Individuals']
        ES_individuals = demo[stat]['Sheltered ES Homeless Individuals']
        TH_individuals = demo[stat]['Sheltered TH Homeless Individuals']
        SH_individuals = demo[stat]['Sheltered SH Homeless Individuals']
        sheltered_total_individuals = demo[stat]['Sheltered Total Homeless Individuals']
        unsheltered_homeless_individuals = demo[stat]['Unsheltered Homeless Individuals']
        homeless_families = demo[stat]['Homeless People in Families']
        ES_families = demo[stat]['Sheltered ES Homeless People in Families']
        TH_families = demo[stat]['Sheltered TH Homeless People in Families']
        sheltered_families = demo[stat]['Sheltered Total Homeless People in Families']
        unsheltered_families = demo[stat]['Unsheltered Homeless People in Families']
        homeless_households = demo[stat]['Homeless Family Households']

        newstat = Demographic(id=id, state=state.abbreviation, year=year, overall=overall, ES=ES, TH=TH, SH=SH, sheltered_total=sheltered_total,
                              unsheltered_homeless=unsheltered_homeless, homeless_individuals=homeless_individuals,
                              ES_individuals=ES_individuals, TH_individuals=TH_individuals,
                              SH_individuals=SH_individuals, sheltered_total_individuals=sheltered_total_individuals,
                              unsheltered_homeless_individuals=unsheltered_homeless_individuals,
                              homeless_families=homeless_families, ES_families=ES_families, TH_families=TH_families,
                              sheltered_families=sheltered_families, unsheltered_families=unsheltered_families, homeless_households=homeless_households)

        db.session.add(newstat)
        db.session.commit()

        index += 1
    db.session.close()

def create_shelters():
    """"
    populating shelters table
    """
    shelters = load_json('Shelters.json')
    index = 1
    for shelter in shelters:
        objectid = index
        ownerRenter = shelter['attributes']['OWNER_RENTER']
        facilityName = shelter['attributes']['FACILITY_NAME']
        provider = shelter['attributes']['PROVIDER']
        address = shelter['attributes']['ADDRESS']
        city = shelter['attributes']['CITY']
        identifer = shelter['attributes']['STATE']
        state = db.session.query(States).filter_by(
            abbreviation=identifer).one()
        zip = shelter['attributes']['ZIP']
        latitude = shelter['attributes']['LATITUDE']
        longitude = shelter['attributes']['LONGITUDE']
        subtype = shelter['attributes']['SUBTYPE']
        status = shelter['attributes']['STATUS']
        numberOfBeds = shelter['attributes']['NUMBER_OF_BEDS']
        agesServed = shelter['attributes']['AGES_SERVED']
        howToAccess = shelter['attributes']['HOW_TO_ACCESS']
        LGBTQServed = shelter['attributes']['LGBTQ_FOCUSED']

        newshelter = Shelters(objectid=objectid, ownerRenter=ownerRenter, facilityName=facilityName, provider=provider,
                              address=address, city=city, state=state.abbreviation, zip=zip, latitude=latitude, longitude=longitude, subtype=subtype,
                              status=status, numberOfBeds=numberOfBeds, agesServed=agesServed, howToAccess=howToAccess, LGBTQServed=LGBTQServed)

        db.session.add(newshelter)
        db.session.commit()
        index += 1
    db.session.close()

def create_channels():
    """"
    populating channels table
    """
    channels = load_json('channels.json')
    index = 1
    for channel in channels:
        games = channel['games']
        id = channel['id']
        language = channel['language']
        title = channel['title']
        username = channel['username']

        newchannel= Channels(games=games, id=id, language=language, title=title, username=username)

        db.session.add(newchannel)
        db.session.commit()
        index += 1
    db.session.close()

# Comment in your call to create when you want to add to the database.
# create_states()
# create_food_banks()    # Has already been created.
# create_demographics()  #already created
# create_shelters()
# create_channels()
# db.session.remove()
