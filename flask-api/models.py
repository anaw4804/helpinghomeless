#!/usr/bin/env python3

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    "DB_STRING", 'postgres://helpinghomeless:cs373team!@helping-homeless-database.cr2exh0kq3y2.us-east-2.rds.amazonaws.com:5432/helpinghomeless_db')
# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

# ------------
# states
# ------------


class States(db.Model):
    """
    States class has 5 attributes.
    name
    abbreviation
    demographis
    shelters
    foodbanks
    """
    __tablename__ = 'states'

    abbreviation = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String(), nullable=False)
    demographics = db.relationship('Demographic', backref='states')
    shelters = db.relationship('Shelters', backref='states')
    foodbanks = db.relationship('FoodBank', backref='states')

# ------------
# FoodBank
# ------------


class FoodBank(db.Model):
    """
    Food bank class has 13 attributes.
    id
    charityName
    rating
    city
    postalCode
    stateOrProvince
    streetAddress1
    streetAddress2
    country
    mission
    tagLine
    websiteURL
    charityNavigatorURL
    """
    __tablename__ = 'foodBank'

    id = db.Column(db.Integer, primary_key=True)
    charityName = db.Column(db.String(), nullable=False)
    charityNavigatorURL = db.Column(db.String())
    rating = db.Column(db.Integer)
    city = db.Column(db.String())
    country = db.Column(db.String())
    postalCode = db.Column(db.String())
    stateOrProvince = db.Column(
        db.String(), db.ForeignKey('states.abbreviation'))
    streetAddress1 = db.Column(db.String())
    streetAddress2 = db.Column(db.String())
    mission = db.Column(db.String())
    tagLine = db.Column(db.String())
    websiteURL = db.Column(db.String())

# ------------
# Demographics
# ------------


class Demographic(db.Model):
    """
    Demographics class has 21 attributes.
    id
    year
    state
    overall
    ES
    TH
    SH
    sheltered_total
    unsheltered_homeless
    homeless_individuals
    ES_individuals
    TH_individuals
    SH_individuals
    sheltered_total_individuals
    unsheltered_homeless_individuals
    homeless_families
    ES_families
    TH_families
    sheltered_families
    unsheltered_families
    homeless_households
    """
    __tablename__ = 'Demographics'
    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.String(), db.ForeignKey('states.abbreviation'))
    year = db.Column(db.String())
    overall = db.Column(db.String())
    ES = db.Column(db.String())
    TH = db.Column(db.String())
    SH = db.Column(db.String())
    sheltered_total = db.Column(db.String())
    unsheltered_homeless = db.Column(db.String())
    homeless_individuals = db.Column(db.String())
    ES_individuals = db.Column(db.String())
    TH_individuals = db.Column(db.String())
    SH_individuals = db.Column(db.String())
    sheltered_total_individuals = db.Column(db.String())
    unsheltered_homeless_individuals = db.Column(db.String())
    homeless_families = db.Column(db.String())
    ES_families = db.Column(db.String())
    TH_families = db.Column(db.String())
    sheltered_families = db.Column(db.String())
    unsheltered_families = db.Column(db.String())
    homeless_households = db.Column(db.String())

# ------------
# Shelters
# ------------


class Shelters(db.Model):
    """
    Shelter class has 16 attributes.
    objectid
    ownerRenter
    facilityName
    provider
    address
    city
    state
    zip
    latitude
    longitude
    subtype
    status
    numberOfBeds
    agesServed
    howToAccess
    LGBTQServed
    """
    __tablename__ = 'shelter'

    objectid = db.Column(db.Integer, primary_key=True)
    ownerRenter = db.Column(db.String(), nullable=False)
    facilityName = db.Column(db.String())
    provider = db.Column(db.String())
    address = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String(), db.ForeignKey('states.abbreviation'))
    zip = db.Column(db.String())
    latitude = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    subtype = db.Column(db.String())
    status = db.Column(db.String())
    numberOfBeds = db.Column(db.Integer)
    agesServed = db.Column(db.String())
    howToAccess = db.Column(db.String())
    LGBTQServed = db.Column(db.String())

class Channels(db.Model):
    """
    Channels class has 5 attributes.
    games
    id
    language
    title
    username
    """
    __tablename__ = 'channel'

    games = db.Column(db.String(), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    language = db.Column(db.String())
    title = db.Column(db.String())
    username = db.Column(db.String())

# db.drop_all() # Will delete tables in database. Do not uncomment.
#db.create_all()
#db.session.remove()
db.session.close()
