import time
from flask import jsonify
import requests
import json

def getjson():
    params = {}
    r = requests.get('https://data.michigan.gov/resource/tgxm-urpt.json?$$app_token=h1PX8vwyLNvzQV83ar3dRBaBx', params=params);
    data = json.loads(r.text)

    stateMapped = {}

    for d in data:
        toModify = {}
        if (d['state'] in stateMapped):
        	toModify = stateMapped[d['state']]

        toModify['year'] = d['year']
        toModify[d['count_type']] = d['count']

        stateMapped[d['state']] = toModify

    print(json.dumps(stateMapped))
getjson()
