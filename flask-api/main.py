# main.py (v1.0)
import time
from flask import Flask, redirect, render_template, jsonify, url_for, request
import requests
import json
import os
from sqlalchemy import cast
from sqlalchemy.types import String
from create_db import app, db, FoodBank, create_food_banks, Shelters, create_shelters, Demographic, create_channels, Channels

app = Flask(__name__, static_folder='static', template_folder='templates')

def handleFoodAPICall(state, rating):
	foodBankList = db.session.query(FoodBank).all()
	db.session.close()
	print('printing food bank list')
	handledJSON = []
	for oneBank in foodBankList:
		cur = {
			'id' : oneBank.id,
	        'charityName' : oneBank.charityName,
	        'charityNavigatorURL' : oneBank.charityNavigatorURL,
	        'rating' : oneBank.rating,
	        'city' : oneBank.city,
	        'country' : oneBank.country,
	        'postalCode' : oneBank.postalCode,
	        'stateOrProvince' : oneBank.stateOrProvince,
	        'streetAddress1' : oneBank.streetAddress1,
	        'streetAddress2' : oneBank.streetAddress2,
	        'mission' : oneBank.mission,
	        'tagLine' : oneBank.tagLine,
	        'websiteURL' : oneBank.websiteURL
		}
		shouldAdd = True
		if state != None and cur['stateOrProvince'] != state :
			shouldAdd = False
		if rating != None and cur['rating'] != rating :
			shouldAdd = False
		if shouldAdd :
			handledJSON.append(cur)

	return handledJSON

def handleDemoAPICall(state):
	demo_list = db.session.query(Demographic).all()
	db.session.close()
	print('printing demographics list')
	handledJSON = []
	for stat in demo_list:
		cur = {
			'id' : stat.id,
	        'state' : stat.state,
	        'year' : stat.year,
			'overall' : stat.overall,
			'ES' : stat.ES,
			'TH' : stat.TH,
			'SH' : stat.SH
 		}
		shouldAdd = True
		if state != None and cur['state'] != state :
			shouldAdd = False
		if shouldAdd :
			handledJSON.append(cur)

	return handledJSON


def handleShelterAPICall(state, rating):
	shelterList = db.session.query(Shelters).all()
	db.session.close()
	print('printing shelters list')
	handledJSON = []
	for shelter in shelterList:
		cur = {
			'objectid' : shelter.objectid,
    		'ownerRenter' : shelter.ownerRenter,
    		'facilityName' : shelter.facilityName,
    		'provider' : shelter.provider,
    		'address' : shelter.address,
			'city' : shelter. city,
			'state' : shelter.state,
			'zip' : shelter.zip,
			'latitude' : shelter.latitude,
			'longitude' : shelter.longitude,
			'subtype' : shelter.subtype,
			'status' : shelter.status,
			'numberOfBeds' : shelter.numberOfBeds,
			'agesServed' : shelter.agesServed,
			'howToAccess' : shelter.howToAccess,
    		'LGBTQServed' : shelter.LGBTQServed
		}
		shouldAdd = True
		if state != None and cur['state'] != state :
			shouldAdd = False
		if rating != None and str(cur['facilityName']) != facilityName :
			shouldAdd = False
		if shouldAdd :
			handledJSON.append(cur)

	return handledJSON

def handleChannelAPICall(id):
	channelList = db.session.query(Channels).all()
	db.session.close()
	print('printing channels list')
	handledJSON = []
	for channel in channelList:
		cur = {
			'games' : channel.games,
			'id' : channel.id,
			'language' : channel.language,
			'title' : channel.title,
			'username' : channel.username
		}
		shouldAdd = True
		if id != None and cur['id'] != id :
			shouldAdd = False
		if shouldAdd :
			handledJSON.append(cur)

	return handledJSON


@app.route('/')
def index():
	return render_template('index.html')

# Correct food api call.
@app.route('/api/food')
def getAllFood():
	state = request.args.get('state') #if key doesn't exist, returns None
	rating = request.args.get('rating')
	return jsonify(handleFoodAPICall(state, rating))

# Displays table of all food banks.
@app.route('/foodBanks', methods=['GET', 'POST'])
def foodResources():
	search = request.args.get('search')
	if 'search' in request.form and request.method == 'POST':
		print(request.form['search'])
		search1 = request.form['search']
		return redirect(url_for('foodResources', search = search1))
	if search == None:
		search = ''
	page = request.args.get('page', 1, type=int)
	print("search: ", search)
	foodBankList = FoodBank.query.filter(FoodBank.charityName.ilike('%{}%'.format(search)) \
	| FoodBank.postalCode.ilike('%{}%'.format(search)) | FoodBank.websiteURL.ilike('%{}%'.format(search)) \
	| FoodBank.tagLine.ilike('%{}%'.format(search)) | FoodBank.streetAddress1.ilike('%{}%'.format(search)) \
	| FoodBank.city.ilike('%{}%'.format(search)) | FoodBank.stateOrProvince.ilike('%{}%'.format(search)) \
	| cast(FoodBank.rating, String).ilike('%{}%'.format(search)) ).paginate(page, 10, False)
	pages = []
	i = 1
	temp = foodBankList
	while(temp.has_next):
		temp = FoodBank.query.filter(FoodBank.charityName.ilike('%{}%'.format(search)) \
		| FoodBank.postalCode.ilike('%{}%'.format(search)) | FoodBank.websiteURL.ilike('%{}%'.format(search)) \
		| FoodBank.tagLine.ilike('%{}%'.format(search)) | FoodBank.streetAddress1.ilike('%{}%'.format(search)) \
		| FoodBank.city.ilike('%{}%'.format(search)) | FoodBank.stateOrProvince.ilike('%{}%'.format(search)) \
		| cast(FoodBank.rating, String).ilike('%{}%'.format(search)) ).paginate(i, 10, False)
		pages.append(url_for('foodResources', search = search, page=i))
		i += 1
	next_url = url_for('foodResources',search = search, page=foodBankList.next_num) \
        if foodBankList.has_next else None
	prev_url = url_for('foodResources',search = search, page=foodBankList.prev_num) \
        if foodBankList.has_prev else None
	return render_template('foodResources.html', foodBankList = foodBankList.items, \
	pages = pages, next_url = next_url, prev_url = prev_url, search = search)

# Page for single food bank.
@app.route('/foodBanks/<int:id>/')
def foodBank(id):
	foodBankRow = FoodBank.query.filter_by(id=id).first()
	return render_template('foodBank.html', foodBank = foodBankRow)

#JSON of shelter data
@app.route('/api/shelters')
def getShelters():
	state = request.args.get('state')
	facilityName = request.args.get('facilityName')
	return jsonify(handleShelterAPICall(state, facilityName))

@app.route('/shelters', methods=['GET', 'POST'])
def shelters():
	search = request.args.get('search')
	if 'search' in request.form and request.method == 'POST':
		print(request.form['search'])
		search1 = request.form['search']
		return redirect(url_for('shelters', search = search1))
	page = request.args.get('page', 1, type=int)
	q =  Shelters.query.filter(Shelters.facilityName.ilike('%{}%'.format(search)) \
	| Shelters.ownerRenter.ilike('%{}%'.format(search)) | Shelters.provider.ilike('%{}%'.format(search)) \
	| Shelters.address.ilike('%{}%'.format(search)) | Shelters.city.ilike('%{}%'.format(search))\
	| Shelters.state.ilike('%{}%'.format(search)) | Shelters.zip.ilike('%{}%'.format(search)))
	if search == None or search == '':
		q = Shelters.query
	shelterList = q.paginate(page, 10, False)
	pages = []
	i = 1
	temp = shelterList
	while(temp.has_next):
		temp = q.paginate(i, 10, False)
		pages.append(url_for('shelters', search = search, page=i))
		i += 1
		print("search: ", search)
	next_url = url_for('shelters',search = search, page=shelterList.next_num) \
        if shelterList.has_next else None
	prev_url = url_for('shelters',search = search, page=shelterList.prev_num) \
        if shelterList.has_prev else None
	return render_template('shelters.html', shelterList = shelterList.items, \
	pages = pages, next_url = next_url, prev_url = prev_url, search = search)

@app.route('/shelters/<int:id>')
def shelter(id):
	shelterRow = Shelters.query.filter_by(objectid=id).first()
	return render_template('shelter.html', shelter = shelterRow)

#JSON of demographics data
@app.route('/api/demographics')
def getDemographics():
	state = request.args.get('state')
	#count_type = request.args.get('count_type')
	return jsonify(handleDemoAPICall(state))

@app.route('/demographics', methods=['GET', 'POST'])
def Demographics():
	search = request.args.get('search')
	if 'search' in request.form and request.method == 'POST':
		print(request.form['search'])
		search1 = request.form['search']
		return redirect(url_for('Demographics', search = search1))
	if search == None:
		search = ''
	page = request.args.get('page', 1, type=int)
	demoList = Demographic.query.filter(Demographic.year.ilike('%{}%'.format(search)) \
	| Demographic.state.ilike('%{}%'.format(search)) | Demographic.overall.ilike('%{}%'.format(search)) \
	| Demographic.ES.ilike('%{}%'.format(search)) | Demographic.TH.ilike('%{}%'.format(search)))\
	.paginate(page, 10, False)
	pages = []
	i = 1
	temp = demoList
	while(temp.has_next):
		temp = Demographic.query.filter(Demographic.year.ilike('%{}%'.format(search)) \
		| Demographic.state.ilike('%{}%'.format(search)) | Demographic.overall.ilike('%{}%'.format(search)) \
		| Demographic.ES.ilike('%{}%'.format(search)) | Demographic.TH.ilike('%{}%'.format(search)))\
		.paginate(i, 10, False)
		pages.append(url_for('Demographics', search = search, page=i))
		i += 1
		print("search: ", search)
	next_url = url_for('Demographics',search = search, page=demoList.next_num) \
        if demoList.has_next else None
	prev_url = url_for('Demographics',search = search, page=demoList.prev_num) \
        if demoList.has_prev else None
	return render_template('demographics.html', data = demoList.items, \
	pages = pages, next_url = next_url, prev_url = prev_url, search = search)

@app.route('/demographics/<int:id>/')
def getDemographic(id):
	stats = db.session.query(Demographic).filter_by(id=id).one()
	db.session.close()
	state = stats.state
	return render_template('demographic.html', data = stats, state = state)

# Correct channels api call.
@app.route('/api/channels')
def getAllChannels():
	id = request.args.get('id') #if key doesn't exist, returns None
	return jsonify(handleChannelAPICall(id))

# Displays table of all channels.
@app.route('/channels', methods=['GET', 'POST'])
def Channelsget():
	search = request.args.get('search')
	if 'search' in request.form and request.method == 'POST':
		print(request.form['search'])
		search1 = request.form['search']
		return redirect(url_for('Channelsget', search = search1))
	page = request.args.get('page', 1, type=int)
	q = Channels.query.filter(Channels.games.ilike('%{}%'.format(search)) \
	| Channels.language.ilike('%{}%'.format(search)) | Channels.title.ilike('%{}%'.format(search)) \
	| Channels.username.ilike('%{}%'.format(search)) \
	| cast(Channels.id, String).ilike('%{}%'.format(search)))
	if search == None or search == '':
		q = Channels.query
	ChannelsList = q.paginate(page, 10, False)
	pages = []
	i = 1
	temp = ChannelsList
	while(temp.has_next):
		temp = q.paginate(i, 10, False)
		pages.append(url_for('Channelsget', search = search, page=i))
		i += 1
		print("search: ", search)
	next_url = url_for('Channelsget',search = search, page=ChannelsList.next_num) \
        if ChannelsList.has_next else None
	prev_url = url_for('Channelsget',search = search, page=ChannelsList.prev_num) \
        if ChannelsList.has_prev else None
	return render_template('Channels.html', ChannelsList = ChannelsList.items, \
	pages = pages, next_url = next_url, prev_url = prev_url, search = search)

# Page for single channel.
@app.route('/channels/<int:id>/')
def channel(id):
	channelRow = Channels.query.filter_by(id=id).first()
	return render_template('channel.html', channel = channelRow)

@app.route('/about', methods=['GET', 'POST'])
def about():
	if request.method == 'POST':
		os.system('python3 -m coverage run --branch test.py > static/results.txt 2>&1')
		os.system('python3 -m coverage report -m >> static/results.txt')
		# Reads the output.
		with open('static/results.txt', 'r') as f:
			return render_template('results.html', text=f.read())
	else:
		return render_template('about.html')

@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.close()

if __name__ == "__main__":
	app.run()
