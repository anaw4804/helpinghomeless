import React from "react";

class PAGE3 extends React.Component {
  render() {
    return (
      <div>
        <p style={{marginTop: "20px"}}>Hello World 3!</p>
      </div>
    );
  }
}

export default PAGE3;
