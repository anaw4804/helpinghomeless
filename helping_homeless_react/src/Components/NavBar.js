import Navbar from "react-bootstrap/Navbar";
import React from "react";
import Nav from "react-bootstrap/Nav";
import logo from "./white-logo.png";
import { BrowserRouter as Route, Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

class NavBar extends React.Component {
  render() {
    return (
        <div>
          <Navbar variant="light" style={{ backgroundColor: "#007bff" }}>
              <Link to="/">
                    <img src={logo} style={{ height: "50px", paddingRight: "30px", paddingLeft: "20px", margin: "10px" }}></img>
              </Link>
            <Nav className="mr-auto">
                <Link style={{ color: "white", paddingRight: "20px", paddingTop: "8px", fontFamily: "now"}} to="/PAGE1">
                    PAGE1
                </Link>
                    <Link style={{ color: "white", paddingRight: "20px", paddingTop: "8px", fontFamily: "now" }} to="/PAGE2">
                    PAGE2
                </Link>
                    <Link style={{ color: "white", paddingRight: "20px", paddingTop: "8px", fontFamily: "now" }} to="/PAGE3">
                    PAGE3
                </Link>
            </Nav>
          </Navbar>
        </div>
    );
  }
}

export default withRouter(NavBar);