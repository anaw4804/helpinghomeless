import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import demo from "./demo.jpg";
import "./HomePage.css";

class HomePage extends React.Component {
    render() {
        return (
          <Container>
            <Row>
              <Col>
                <Row>
                  <Col md className="gray-box">
                     alia partem no, haru
                  </Col>
                </Row>
                <Row>
                  <Col className="dark-box">equidem</Col>
                  <Col className="light-box" />
                </Row>
                <Row>
                  <Col className="dark-box secondary">
                      Lorem ipsum dolor sit amet, ut tollit gloriatur sit, in nisl stet omittantur ius. Ea est utroque urbanitas instructior, eu natum ubique definitiones est. Ex nobis blandit omnesque eos, quo id case
                  </Col>
                </Row>
              </Col>
              <Col lg style={{marginBottom: "10px"}}>
                <img src={demo} style={{ width: "100%" }} />
              </Col>
            </Row>
            <Row style={{ marginTop: "50px" }}>
              <Col className="dark-box alt">
                  TAB1
              </Col>
              <Col className="light-box alt">
                  TAB2
              </Col>
              <Col className="dark-box alt">
                  TAB3
              </Col>
            </Row>
          </Container>
        );
    }
};

export default HomePage;