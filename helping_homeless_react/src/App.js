import './App.css';

import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";

import NavBar from "./Components/NavBar";
import HomePage from "./Components/HomePage";
import PAGE1 from "./Components/PAGE1";
import PAGE2 from './Components/PAGE2';
import PAGE3 from "./Components/PAGE3";

const Main = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route exact path="/PAGE1" component={PAGE1} />
    <Route exact path="/PAGE2" component={PAGE2} />
    <Route exact path="/PAGE3" component={PAGE3} />
  </Switch>
);

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <header className="App-header">
          <Main />
        </header>
        <p>My Token = {window.token}</p>
      </div>
    );
  }
}

export default App;
